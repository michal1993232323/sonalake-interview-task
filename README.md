## PRECONDITIONS

In order to start using this application you need to log in to `https://www.weatherbit.io/account/login` and 
generate you own api key. Then paste it in `application.yml` file.

## RUN APPLICATION

In order to run this application please invoke `bootRun` gradle task.\
After that application will be running on `http://localhost:8080`.
If you want to start forecasting make GET request on: 
```
http://localhost:8080/{forecastDay} where forecastDay in yyyy-MM-dd format
``` 