package com.sonalake.weatherservice.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sonalake.weatherservice.model.Forecast;
import com.sonalake.weatherservice.testcollections.TestForecastCollection;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.test.StepVerifier;

import java.io.IOException;
import java.time.LocalDate;

import static java.util.Collections.singletonList;

@ExtendWith(SpringExtension.class)
@Import(ObjectMapper.class)
class ForecastServiceTest {

    private static MockWebServer mockWebServer;

    private static ForecastService forecastService;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeAll
    static void setUp() throws IOException {
        mockWebServer = new MockWebServer();
        mockWebServer.start();
        forecastService = new ForecastService(WebClient.create("http://localhost:" + mockWebServer.getPort()));
    }

    @Test
    @DisplayName("should return forecasts")
    void forecasts() throws JsonProcessingException {
        // given
        final Forecast expectedForecast =
                TestForecastCollection.createForecast("Cracow", 1.1, 1.2, LocalDate.now());
        mockWebServer.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(expectedForecast))
                .addHeader("Content-Type", "application/json"));

        // when
        // then
        StepVerifier.create(forecastService.getForecastForLocations(singletonList("Cracow")))
                .expectNext(expectedForecast)
                .verifyComplete();
    }

    @AfterAll
    static void tearDown() throws IOException {
        mockWebServer.shutdown();
    }
}
