package com.sonalake.weatherservice.service;

import com.sonalake.weatherservice.config.WeatherbitProperties;
import com.sonalake.weatherservice.config.WeatherbitProperties.Value;
import com.sonalake.weatherservice.model.Forecast;
import com.sonalake.weatherservice.model.Location;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sonalake.weatherservice.testcollections.TestForecastCollection.createForecast;
import static java.util.Arrays.asList;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@Import(LocationService.class)
class LocationServiceTest {

    @MockBean
    private ForecastService forecastService;

    @MockBean
    private WeatherbitProperties weatherbitProperties;

    @Autowired
    private LocationService locationService;

    @Test
    @DisplayName("should return best windsurfing location")
    void bestLocation() {
        // given
        final List<String> locations = asList("Cracow", "Warsaw");
        final LocalDate validDate = LocalDate.of(1993, 1, 1);
        final List<Forecast> forecasts = asList(
                createForecast(locations.get(1), 23, 18, validDate),
                createForecast(locations.get(0), 11.4, 14, validDate)
        );
        final Map<String, Value> properties = new HashMap<>();
        properties.put("temperature", new Value(BigDecimal.valueOf(5), BigDecimal.valueOf(35)));
        properties.put("wind", new Value(BigDecimal.valueOf(5), BigDecimal.valueOf(18)));

        when(weatherbitProperties.getLocations()).thenReturn(locations);
        when(forecastService.getForecastForLocations(locations))
                .thenReturn(Flux.fromIterable(forecasts).parallel());
        when(weatherbitProperties.getParameters()).thenReturn(properties);

        final Location expectedLocation = new Location("Warsaw", BigDecimal.valueOf(23.0), BigDecimal.valueOf(18.0));

        // when
        // then
        StepVerifier.create(locationService.getBestWindsurfingLocationForDay(validDate))
                .expectNext(expectedLocation)
                .verifyComplete();
    }

    @Test
    @DisplayName("should return default location")
    void defaultLocation() {
        // given
        final List<String> locations = asList("Cracow", "Warsaw");
        final LocalDate validDate = LocalDate.of(1993, 1, 1);
        final List<Forecast> forecasts = asList(
                createForecast(locations.get(1), 50, 18, validDate),
                createForecast(locations.get(0), 55, 14, validDate)
        );
        final Map<String, Value> properties = new HashMap<>();
        properties.put("temperature", new Value(BigDecimal.valueOf(5), BigDecimal.valueOf(35)));
        properties.put("wind", new Value(BigDecimal.valueOf(5), BigDecimal.valueOf(18)));

        when(weatherbitProperties.getLocations()).thenReturn(locations);
        when(forecastService.getForecastForLocations(locations))
                .thenReturn(Flux.fromIterable(forecasts).parallel());
        when(weatherbitProperties.getParameters()).thenReturn(properties);

        // when
        // then
        StepVerifier.create(locationService.getBestWindsurfingLocationForDay(validDate))
                .expectNext(Location.defaultLocation())
                .verifyComplete();
    }
}
