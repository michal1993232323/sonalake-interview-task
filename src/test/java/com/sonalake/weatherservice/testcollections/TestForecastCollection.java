package com.sonalake.weatherservice.testcollections;

import com.sonalake.weatherservice.model.Forecast;
import com.sonalake.weatherservice.model.ForecastDetails;

import java.time.LocalDate;

import static java.math.BigDecimal.valueOf;
import static java.util.Collections.singletonList;

public class TestForecastCollection {

    public static Forecast createForecast(
            final String cityName,
            final double avgTemperature,
            final double windSpeed,
            final LocalDate validDate) {
        return new Forecast(cityName, singletonList(new ForecastDetails(valueOf(avgTemperature), valueOf(windSpeed), validDate)));
    }
}
