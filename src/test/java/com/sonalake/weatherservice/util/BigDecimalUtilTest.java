package com.sonalake.weatherservice.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.provider.CsvSource;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class BigDecimalUtilTest {

    @ParameterizedTest
    @DisplayName("should verify if big decimal between range")
    @CsvSource({
            "20, 5, 25, true",
            "20, 5, 20, true",
            "5, 5, 20, true",
            "4, 5, 20, false"
    })
    void isBetweenInclusive(final ArgumentsAccessor arguments) {
        // given
        // when
        final boolean actual = BigDecimalUtil.isBetweenInclusive(
                arguments.get(0, BigDecimal.class),
                arguments.get(1, BigDecimal.class),
                arguments.get(2, BigDecimal.class));

        // then
        assertThat(actual).isEqualTo(arguments.getBoolean(3));
    }
}
