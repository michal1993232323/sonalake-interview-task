package com.sonalake.weatherservice.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

class LocalDateUtilTest {

    @ParameterizedTest
    @DisplayName("should verify if local date between range")
    @CsvSource({
            "2020-10-10, 2020-10-20, 2020-10-10, true",
            "2020-10-10, 2020-10-20, 2020-10-20, true",
            "2020-10-10, 2020-10-20, 2020-10-21, false",
            "2020-10-10, 2020-10-20, 2020-10-15, true",
    })
    void isBetweenInclusive(final ArgumentsAccessor arguments) {
        // given
        // when
        final boolean actual = LocalDateUtil.isBetweenInclusive(
                arguments.get(0, LocalDate.class),
                arguments.get(1, LocalDate.class),
                arguments.get(2, LocalDate.class));

        // then
        assertThat(actual).isEqualTo(arguments.getBoolean(3));
    }
}
