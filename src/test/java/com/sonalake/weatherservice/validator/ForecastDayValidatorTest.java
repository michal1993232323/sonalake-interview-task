package com.sonalake.weatherservice.validator;

import com.sonalake.weatherservice.exception.ForecastNotWithin15DaysException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static com.sonalake.weatherservice.validator.ForecastDayValidator.isWithin15Days;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class ForecastDayValidatorTest {

    @Test
    @DisplayName("should throw exception if date not within 15 day from now")
    void exception() {
        // given
        final LocalDate forecastDay = LocalDate.now().plusDays(20);

        // when
        // then
        assertThatExceptionOfType(ForecastNotWithin15DaysException.class).isThrownBy(() -> isWithin15Days(forecastDay));
    }

    @Test
    @DisplayName("should pass validation if date withing 15 days from now")
    void noException() {
        // given
        final LocalDate forecastDay = LocalDate.now().plusDays(10);

        // when
        // then
        assertThatCode(() -> isWithin15Days(forecastDay)).doesNotThrowAnyException();
    }
}
