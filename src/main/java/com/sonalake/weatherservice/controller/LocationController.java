package com.sonalake.weatherservice.controller;

import com.sonalake.weatherservice.model.Location;
import com.sonalake.weatherservice.service.LocationService;
import com.sonalake.weatherservice.validator.ForecastDayValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

import static org.springframework.format.annotation.DateTimeFormat.ISO;

@RestController
@RequiredArgsConstructor
public class LocationController {

    private final LocationService locationService;

    @GetMapping("/{forecastDay}")
    public ResponseEntity<Mono<Location>> getBestWindsurfingLocationForDay(
            @DateTimeFormat(iso = ISO.DATE, pattern = "yyyy-MM-dd") @PathVariable final LocalDate forecastDay) {
        ForecastDayValidator.isWithin15Days(forecastDay);
        return ResponseEntity.ok(locationService.getBestWindsurfingLocationForDay(forecastDay));
    }
}
