package com.sonalake.weatherservice.config;

import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

import static io.netty.channel.ChannelOption.CONNECT_TIMEOUT_MILLIS;

@Configuration
@RequiredArgsConstructor
public class WebClientConfig {

    private final WeatherbitProperties properties;

    @Bean
    public WebClient webClient() {

        final HttpClient httpClient = HttpClient.create()
                .tcpConfiguration(client ->
                        client.option(CONNECT_TIMEOUT_MILLIS, properties.getConnectionTimeout())
                                .doOnConnected(connection -> connection
                                        .addHandlerLast(new ReadTimeoutHandler(properties.getReadTimeout()))
                                        .addHandlerLast(new WriteTimeoutHandler(properties.getWriteTimeout()))));

        final ClientHttpConnector connector = new ReactorClientHttpConnector(httpClient);

        return WebClient.builder()
                .baseUrl(properties.getBaseUrl())
                .clientConnector(connector)
                .build();
    }
}
