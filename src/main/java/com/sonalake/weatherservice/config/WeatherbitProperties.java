package com.sonalake.weatherservice.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "weatherbit")
public class WeatherbitProperties {

    private String apiKey;
    private List<String> locations;
    private String baseUrl;
    private int connectionTimeout;
    private int readTimeout;
    private int writeTimeout;
    private Map<String, Value> parameters;

    @Getter
    @Setter
    @AllArgsConstructor
    public static class Value {

        private BigDecimal min;
        private BigDecimal max;
    }
}
