package com.sonalake.weatherservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ForecastDetails {

    @JsonProperty("temp")
    private BigDecimal avgTemperature;
    @JsonProperty("wind_spd")
    private BigDecimal windSpeed;
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonProperty("valid_date")
    private LocalDate validDate;
}
