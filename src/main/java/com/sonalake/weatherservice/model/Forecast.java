package com.sonalake.weatherservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Forecast {

    @JsonProperty("city_name")
    private String cityName;
    @JsonProperty("data")
    private List<ForecastDetails> forecastDetails;
}
