package com.sonalake.weatherservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Location {

    private String name;
    private BigDecimal avgTemperature;
    private BigDecimal windSpeed;

    public static Location defaultLocation() {
        return new Location("", BigDecimal.ZERO, BigDecimal.ZERO);
    }
}
