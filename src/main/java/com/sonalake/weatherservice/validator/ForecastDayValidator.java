package com.sonalake.weatherservice.validator;

import com.sonalake.weatherservice.exception.ForecastNotWithin15DaysException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

import static com.sonalake.weatherservice.util.LocalDateUtil.isBetweenInclusive;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ForecastDayValidator {

    public static final int FORECAST_FOR_PERIOD = 15;

    public static void isWithin15Days(final LocalDate forecastDay) {
        final LocalDate startDate = LocalDate.now();
        final LocalDate endDate = startDate.plusDays(FORECAST_FOR_PERIOD);
        if (!isBetweenInclusive(startDate, endDate, forecastDay)) {
            throw new ForecastNotWithin15DaysException("application supports only " + FORECAST_FOR_PERIOD + " days forecast");
        }
    }
}
