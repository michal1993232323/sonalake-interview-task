package com.sonalake.weatherservice.comparator;

import com.sonalake.weatherservice.model.ForecastDetails;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Comparator;

import static java.util.Comparator.comparing;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ForecastDetailsComparator {

    private static final BigDecimal THREE = BigDecimal.valueOf(3);

    private static final Comparator<ForecastDetails> COMPARATOR_BY_FORMULA = comparing(ForecastDetailsComparator::formula);

    public static Comparator<ForecastDetails> getComparatorByFormula() {
        return COMPARATOR_BY_FORMULA;
    }

    private static BigDecimal formula(final ForecastDetails details) {
        return details.getWindSpeed().multiply(THREE).add(details.getAvgTemperature());
    }
}
