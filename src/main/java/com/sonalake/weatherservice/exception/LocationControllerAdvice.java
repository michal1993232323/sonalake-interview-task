package com.sonalake.weatherservice.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static com.sonalake.weatherservice.validator.ForecastDayValidator.FORECAST_FOR_PERIOD;

@Slf4j
@ControllerAdvice
public class LocationControllerAdvice {

    @ExceptionHandler(ForecastNotWithin15DaysException.class)
    public ResponseEntity<ApiError> handleForecastNotWithin15DaysException(final ForecastNotWithin15DaysException ex) {
        log.info("forecast date is not within " + FORECAST_FOR_PERIOD + " days from now", ex);
        return ResponseEntity.badRequest().body(ApiError.of(ex.getMessage(), "001"));
    }

    @ExceptionHandler(ConversionFailedException.class)
    public ResponseEntity<ApiError> handleConversionFailedException(final ConversionFailedException ex) {
        log.info("request parameter in incorrect format", ex);
        return ResponseEntity.badRequest().body(ApiError.of(ex.getMessage(), "002"));
    }

    @Getter
    @Setter
    @RequiredArgsConstructor(staticName = "of")
    public static class ApiError {

        private final String errorMessage;
        private final String errorCode;
    }
}
