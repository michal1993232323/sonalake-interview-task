package com.sonalake.weatherservice.exception;

public class ForecastNotWithin15DaysException extends RuntimeException {

    public ForecastNotWithin15DaysException(String message) {
        super(message);
    }
}
