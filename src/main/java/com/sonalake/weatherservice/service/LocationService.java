package com.sonalake.weatherservice.service;

import com.sonalake.weatherservice.config.WeatherbitProperties;
import com.sonalake.weatherservice.config.WeatherbitProperties.Value;
import com.sonalake.weatherservice.model.Forecast;
import com.sonalake.weatherservice.model.ForecastDetails;
import com.sonalake.weatherservice.model.Location;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.time.LocalDate;
import java.util.function.Predicate;

import static com.sonalake.weatherservice.comparator.ForecastDetailsComparator.getComparatorByFormula;
import static com.sonalake.weatherservice.model.Location.defaultLocation;
import static com.sonalake.weatherservice.util.BigDecimalUtil.isBetweenInclusive;

@Service
@RequiredArgsConstructor
public class LocationService {

    private final ForecastService forecastService;
    private final WeatherbitProperties properties;

    public Mono<Location> getBestWindsurfingLocationForDay(final LocalDate day) {
        return forecastService.getForecastForLocations(properties.getLocations())
                .map(forecast -> forecastToTuple(forecast, day))
                .filter(tuple -> isPerfectWeather().test(tuple.getT2()))
                .sorted((t1, t2) -> getComparatorByFormula().compare(t1.getT2(), t2.getT2()))
                .map(this::tupleToLocation)
                .last(defaultLocation());
    }

    private Predicate<ForecastDetails> isPerfectTemperature() {
        final Value temperature = properties.getParameters().get("temperature");
        return details -> isBetweenInclusive(details.getAvgTemperature(), temperature.getMin(), temperature.getMax());
    }

    private Predicate<ForecastDetails> isPerfectWind() {
        final Value wind = properties.getParameters().get("wind");
        return details -> isBetweenInclusive(details.getWindSpeed(), wind.getMin(), wind.getMax());
    }

    private Predicate<ForecastDetails> isPerfectWeather() {
        return details -> isPerfectTemperature().and(isPerfectWind()).test(details);
    }

    private ForecastDetails getDetailsByDay(final Forecast forecast, final LocalDate day) {
        return forecast.getForecastDetails()
                .stream()
                .filter(details -> details.getValidDate().equals(day))
                .findFirst()
                .get(); // without checking because `findFirst` won't be empty
    }

    private Tuple2<String, ForecastDetails> forecastToTuple(final Forecast forecast, final LocalDate day) {
        return Tuples.of(forecast.getCityName(), getDetailsByDay(forecast, day));
    }

    private Location tupleToLocation(final Tuple2<String, ForecastDetails> tuple) {
        return new Location(tuple.getT1(), tuple.getT2().getAvgTemperature(), tuple.getT2().getWindSpeed());
    }
}
