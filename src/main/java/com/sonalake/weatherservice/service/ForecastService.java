package com.sonalake.weatherservice.service;

import com.sonalake.weatherservice.model.Forecast;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.ParallelFlux;
import reactor.core.scheduler.Schedulers;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ForecastService {

    @Value("${weatherbit.apikey}")
    private String apiKey;
    private final WebClient webClient;

    public ParallelFlux<Forecast> getForecastForLocations(final List<String> locations) {
        return Flux.fromIterable(locations)
                .parallel()
                .runOn(Schedulers.elastic())
                .flatMap(this::getForecastForLocation);
    }

    private Mono<Forecast> getForecastForLocation(final String location) {
        return webClient
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path("/forecast/daily")
                        .queryParam("city", location)
                        .queryParam("key", apiKey)
                        .build())
                .retrieve()
                .bodyToMono(Forecast.class);
    }
}
