package com.sonalake.weatherservice.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BigDecimalUtil {

    public static boolean isBetweenInclusive(final BigDecimal value, final BigDecimal start, final BigDecimal end) {
        return value.compareTo(start) >= 0 && value.compareTo(end) <= 0;
    }
}
